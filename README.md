Vending Machine Kata (in Java 8)
================================

My solution to [guyroyse/vending-machine-kata](https://github.com/guyroyse/vending-machine-kata) problem.

I TDD'd this.

### NOTES
avg weight of quarter = 5.670 grams (5.6 - 5.7)
avg size of quarter = diameter is 0.955 inches (.95 - .96)

avg weight of dime = 2.28 gram (2.2 - 2.3)
avg size of dime = .705 inches (.7 - .71)

avg weight of nickel = 5.01 grams (4.95 - 5.05)
avg size of nickel = .835 inches (.83 - .84)
